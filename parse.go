package main

import (
	"database/sql"
	"encoding/csv"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"sync"

	_ "github.com/lib/pq"
)

var inputFilename string
var workers int

func init() {
	flag.StringVar(&inputFilename, "input", "-", "input. Default '-' (stdin)")
	flag.IntVar(&workers, "workers", 1, "Number of workers")
}

func main() {
	flag.Parse()
	if err := do(); err != nil {
		log.Fatal(err.Error())
	}
}

func do() error {
	source, err := getSource(inputFilename)
	if err != nil {
		return err
	}

	db, err := sql.Open("postgres", "")
	if err != nil {
		return err
	}

	if closer, ok := source.(io.Closer); ok {
		defer closer.Close()
	}

	chOrders, err := readCSV(source)
	if err != nil {
		return err
	}

	wg := sync.WaitGroup{}

	for i := 0; i < workers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for order := range chOrders {
				if err := dbInsert(db, order); err != nil {
					log.Printf("Error inserting order %s: %s\n", order.ID, err.Error())
				}
			}
		}()
	}
	wg.Wait()

	return nil
}

func dbInsert(db *sql.DB, order Order) error {
	_, err := db.Exec(`INSERT INTO customer_order
		(id, customer, item, quantity)
		VALUES ($1, $2, $3, $4)`,
		order.ID,
		order.CustomerID,
		order.Item,
		order.Quantity)

	return err
}

// getSource returns a reader from a URI, '-' is stdin, a valid URL will be
// downloaded to a temp file then returned, or a local file will be opened
func getSource(uri string) (io.Reader, error) {
	if uri == "-" {
		return os.Stdin, nil
	}

	if isHTTPURL(uri) {
		return downloadedFile(uri)
	}

	return os.Open(uri)
}

func isHTTPURL(uri string) bool {
	if parsed, err := url.Parse(uri); err == nil && (parsed.Scheme == "http" || parsed.Scheme == "https") {
		return true
	}
	return false

}

// downloadedFile returns an io.Reader pointing to a local downloaded copy of a
// remote URL
func downloadedFile(uri string) (io.Reader, error) {
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return nil, err
	}

	temp, err := ioutil.TempFile("", "")
	if err != nil {
		return nil, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	io.Copy(temp, res.Body)
	temp.Seek(0, 0)
	return temp, nil
}

// Order represents a customer order
type Order struct {
	ID         string
	CustomerID string
	Item       string
	Quantity   float64
}

// readCSV reads a header row, then parses orders, sending them to the returned
// chan
func readCSV(reader io.Reader) (<-chan Order, error) {
	r := csv.NewReader(reader)
	ch := make(chan Order, 10)

	headerRecord, err := r.Read()
	if err != nil {
		return nil, err
	}

	go func() {
		for lineNumber := 2; true; lineNumber++ {
			record, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				if csvErr, ok := err.(*csv.ParseError); ok {
					log.Println(csvErr.Error())
					continue
				}
				log.Println(err.Error())
				break
			}

			recordMap := map[string]string{}
			for idx, key := range headerRecord {
				recordMap[key] = record[idx]
			}

			order, err := mapToOrder(recordMap)
			if err != nil {
				log.Printf("Line %d: %s\n", lineNumber, err.Error())
				continue
			}

			ch <- order
		}

		close(ch)
	}()

	return ch, nil
}

// mapToOrder returns an order from a CSV object map of strings
func mapToOrder(in map[string]string) (Order, error) {
	order := Order{
		ID:         in["orderId"],
		CustomerID: in["customerId"],
		Item:       in["item"],
	}
	var err error
	order.Quantity, err = strconv.ParseFloat(in["quantity"], 64)
	return order, err
}
