package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

var testData = `orderId,customerId,item,quantity
1,2,3,4
2,2,2,2
bad
order,customer,item,string
3,3,3,3
`

func TestReadCSV(t *testing.T) {
	reader := strings.NewReader(testData)

	collected := make([]Order, 0, 0)
	chOrders, err := readCSV(reader)
	if err != nil {
		t.Error(err)
	}

	for line := range chOrders {
		collected = append(collected, line)
	}

	if got := len(collected); got != 3 {
		t.Errorf("Expected 3 rows, got %d", got)
	} else {
		first := collected[0]
		if first.ID != "1" {
			t.Errorf("Want ID 1, got %s", first.ID)
		}

	}
}

func TestHTTPURL(t *testing.T) {
	for _, in := range []string{
		"https://www.example.com",
		"http://example.com",
		"HTTPS://WWW.EXAMPLE.COM",
		"http://thing.co.uk",
		"http://thing.xyz",
	} {
		if !isHTTPURL(in) {
			t.Errorf("Wasn't: %s", in)
		}
	}
	for _, in := range []string{
		"string",
		"-",
		" http://thing.com",
		"file://./thing",
	} {
		if isHTTPURL(in) {
			t.Errorf("Was: %s", in)
		}
	}
}

func TestDownloadedFile(t *testing.T) {

	s := httptest.NewServer(http.HandlerFunc(
		func(rw http.ResponseWriter, req *http.Request) {
			rw.Write([]byte(testData))
		}))

	localFile, err := downloadedFile(s.URL)
	if err != nil {
		t.Fatalf(err.Error())
	}

	content, err := ioutil.ReadAll(localFile)
	if err != nil {
		t.Fatalf(err.Error())
	}

	if strContent := string(content); strContent != testData {
		t.Errorf("Bad Content")
	}

}
